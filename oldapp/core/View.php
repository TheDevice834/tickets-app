<?php
namespace core;

class View {
    private $variables = [];

    public function __cunstruct(){

    }

    public function render($template, $variables = []){
        $this->variables = $variables;

        extract($this->variables);
        ob_start();
        require TPL.'/'.$template.'.phtml';
        $content = ob_get_clean();

        echo $content;
        return true;
    }

    private function inctpl($template){
        extract($this->variables);
        require TPL.'/'.$template.'.phtml';
    }
}