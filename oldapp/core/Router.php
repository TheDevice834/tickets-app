<?php
namespace core;

class Router {
    private $uri;
    private $routes;
    private $matched;
    private $variables;

    public function __construct($uri){
        $this->uri = explode('?', $uri)[0];

        require APP.'/routes.php';
    }

    private function find(){
        foreach($this->routes as $pattern => $action){
            if(preg_match('~^'.$pattern.'$~', $this->uri, $variables)){
                $this->matched = $action;
                $this->variables = $this->clearVariables($variables);

                return true;
            } else continue;
        }
        
        return false;
    }

    public function load(){
        if(!$this->find()) notFound();

        $className = $this->matched['controller'];
        $method = $this->matched['method'];
        $variables = $this->variables;

        $className = 'app\\controllers\\'.$className;

        $obj = new $className();
        $obj->$method(...$variables);
    }

    private function add($pattern, $action){
        $controller = explode('@', $action)[0];
        $method = explode('@', $action)[1];

        $this->routes[$pattern] = [
            'controller' => $controller,
            'method' => $method
        ];
    }

    private function clearVariables($variables){
        foreach($variables as $key => $variable){
            if(!is_int($key)) unset($variables[$key]);
        }
        unset($variables[0]);

        return $variables;
    }
}