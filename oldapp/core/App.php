<?php
namespace core;

class App {
    private $router;

    public function __construct(){
        $this->router = new Router($_SERVER['REQUEST_URI']);
    }

    public function run(){
        $this->router->load();
    }
}