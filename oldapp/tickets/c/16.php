<?php
return [
    'questions' => [
        1 => [
            'question' => 'В каком ответе наиболее точно указаны узлы и детали, входящие в систему питания дизельного двигателя?',
            'answers' => [
                1 => 'Топливный бак, форсунки, топливоподкачивающий насос, глушитель, фильтры грубой и тонкой очистки топлива, топливопроводы низкого и высокого давления, выпускной трубопровод.',
                2 => 'Топливный бак, топливоподкачивающий насос, фильтры грубой и тонкой очистки топлива, насос высокого давления, форсунки, топливопроводы низкого и высокого давления, выпускной трубопровод, глушитель.',
            ]
        ], 2 => [
            'question' => 'Для чего предназначен газораспределительный механизм двигателя?',
            'answers' => [
                1 => 'Для своевременного запуска газов и наполнения цилиндров свежим зарядом (горючей смесью).',
                2 => 'Для своевременного выпуска отработавших газов и наполнения цилиндров свежим зарядом.',
                3 => 'Своевременного выпуска отработавших газов и наполнения цилиндров свежим зарядом и воздухом.',
                4 => 'Своевременного выпуска отработавших газов и наполнения цилиндров свежим воздухом.'
            ]
        ], 3 => [
            'question' => 'Как называется поступающая в цилиндры смесь распыленного и частично испаренного топлива с воздухом, смешанная внутри цилиндра с отработавшими газами?',
            'answers' => [
                1 => 'Обогащенная смесь.',
                2 => 'Горючая смесь.',
                3 => 'Рабочая смесь.',
                4 => 'Обедненная смесь.'
            ]
        ], 4 => [
            'question' => 'Какой цифрой обозначен промежуточный вал коробки передач?',
            'answers' => [
                1 => '1',
                2 => '2',
                3 => '3',
                4 => '4'
            ],
            'image' => '16-4'
        ], 5 => [
            'question' => 'Какая коробка передач устанавливается на автомобиле МАЗ-5551?',
            'answers' => [
                1 => '5-ступенчатая.',
                2 => '10-ступенчатая.',
                3 => '10-ступенчатая, состоящая из 5-ступенчатой коробки передач и ускоряющего делителя.',
            ],
            'image' => '16-5'
        ], 6 => [
            'question' => 'Для чего в механизме переключения передач имеются фиксаторы?',
            'answers' => [
                1 => 'Для предотвращения включения 2-х передач одновременно.',
                2 => 'Для предотвращения самопроизвольного выключения передач.',
                3 => 'Оба ответа правильные.',
            ]
        ], 7 => [
            'question' => 'Для чего предназначена аккумуляторная батарея?',
            'answers' => [
                1 => 'Для питания электрическим током стартера и иных потребителей при неработающем генераторе.',
                2 => 'Для питания потребителей совместно с генератором, когда потребляемая сила тока превышает максимально допустимую для генератора.',
                3 => 'Для выполнения всех вышеперечисленных функций.',
            ]
        ], 8 => [
            'question' => 'Как восполняется убыль антифриза (тосола) в результате утечки из системы охлаждения двигателя?',
            'answers' => [
                1 => 'Добавляется дистиллированная вода.',
                2 => 'Добавляется антифриз (тосол).',
                3 => 'Добавляется антифриз (тосол) разбавленный дистиллированной водой.',
            ]
        ], 9 => [
            'question' => 'Какое минимальное количество противооткатных упоров должно находиться в грузовом автомобилесамосвале МАЗ-5551?',
            'answers' => [
                1 => 'Один.',
                2 => 'Два.',
                3 => 'Три.',
                4 => 'Четыре.'
            ]
        ], 10 => [
            'question' => 'При каком виде технического обслуживания необходимо производить замену масла в двигателе?',
            'answers' => [
                1 => 'При ТО-1.',
                2 => 'При ТО-2.',
                3 => 'При сезонном обслуживании.',
            ]
        ]
    ],
    'right' => [
        1 => 2,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 1,
        6 => 3,
        7 => 3,
        8 => 2,
        9 => 2,
        10 => 2
    ]
];