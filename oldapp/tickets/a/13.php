<?php
return [
    'questions' => [
        1 => [
            'question' => 'Какова переодичность проведения ТО-2 тракторов БЕЛАРУС-80.1?',
            'answers' => [
                1 => '125 часов.',
                2 => '500 часов.',
                3 => '1000 часов.',
                4 => '2000 часов.'
            ]
        ], 2 => [
            'question' => 'По какой из указанных причин двигатель Д-243 идет в &laquo;разнос&raquo;',
            'answers' => [
                1 => 'Заклинивание рейки топливного насоса.',
                2 => 'Наличие воды в топливе.',
                3 => 'Недостаточное количество воды в системе охлаждения.'
            ]
        ], 3 => [
            'question' => 'До какого уровня заполняют систему охлаждения двигателя трактора БЕЛАРУС-80.1 охлаждающей жидкостью?',
            'answers' => [
                1 => 'На 50...60 мм ниже верхнего торца заливной горловины верхнего бака радиатора.',
                2 => 'Сердцевина радиатора должна быть закрыта водой.',
                3 => 'До половины верхнего бака.'
            ]
        ], 4 => [
            'question' => 'Какой нормальный свободный ход педали сцепления трактора БЕЛАРУС-82.1 (по подушке педали)?',
            'answers' => [
                1 => '10...15 мм.',
                2 => '40...50 мм.',
                3 => '60...80 мм.'
            ]
        ], 5 => [
            'question' => 'При каких оборотах двигателя положено включать независимый привод ВОМ у трактора БЕЛАРУС-82.1?',
            'answers' => [
                1 => 'При минимальных оборотах.',
                2 => 'При максимальных оборотах.',
                3 => 'Обороты двигателя не имеют значения.'
            ]
        ], 6 => [
            'question' => 'В каком из указанных случаев, автоматическая блокировка дифференциала трактора МТЗ-80 выключается автоматически?',
            'answers' => [
                1 => 'При скорости более 30 км/час.',
                2 => 'При работе на склонах.',
                3 => 'При транспортных работах.',
                4 => 'При повороте передних колес на угол более 8&deg;.'
            ]
        ], 7 => [
            'question' => 'Какая установка ведущих колес с грунтозацепами, имеющими вид елочки, считается правильной?',
            'answers' => [
                1 => '<img src="/assets/img/tickets/a/1.png" width="75">',
                2 => '<img src="/assets/img/tickets/a/2.png" width="75">',
                3 => 'Расположение грунтозацепов не имеет значения.'
            ]
        ], 8 => [
            'question' => 'Для чего в системе охлаждения двигателя предназначен термостат?',
            'answers' => [
                1 => 'Для сообщения внутренней полости радиатора с атмосферой.',
                2 => 'Для поддержания оптимального температурного режима двигателя.'
            ]
        ], 9 => [
            'question' => 'Какое из перечисленных масел применяется в системе смазки двигателя Д-243 при его эксплуатации в зимний период?',
            'answers' => [
                1 => 'М8Г<sub>2</sub>.',
                2 => 'М10Г<sub>2</sub>.',
                3 => 'ТЭП-15.',
            ]
        ], 10 => [
            'question' => 'Какова продолжительность обкатки нового трактора БЕЛАРУС-80.1?',
            'answers' => [
                1 => '5...10 ч.',
                2 => '10...15 ч.',
                3 => 'Не менее 30 ч.'
            ]
        ]
    ],
    'right' => [
        1 => 2,
        2 => 1,
        3 => 1,
        4 => 2,
        5 => 1,
        6 => 4,
        7 => 1,
        8 => 2,
        9 => 1,
        10 => 3
    ]
];