<?php
$dirs = scandir('../tickets', true);
unset($dirs[count($dirs) - 1], $dirs[count($dirs) - 1]);

$tickets = [];
foreach($dirs as $dir){
  $files = scandir('../tickets/'.$dir, true);
  unset($files[count($files) - 1], $files[count($files) - 1]);
  array_multisort($files, SORT_NUMERIC);
  
  foreach($files as $fileKey => $file){
    $tickets[$dir][explode('.', $file)[0]] = include('../tickets/'.$dir.'/'.$file);
  }
}

file_put_contents('tickets.json', json_encode($tickets));