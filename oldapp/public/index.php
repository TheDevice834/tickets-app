<?php
session_start();

define('ROOT', dirname(__DIR__));
define('PUBLIC', ROOT.'/public');
define('APP', ROOT.'/app');
define('TPL', ROOT.'/templates');

require APP.'/functions.php';

spl_autoload_register(function($className){
    $classPath = str_replace('\\', '/', $className);
    $classPath = ROOT.'/'.$classPath.'.php';
    require $classPath;
});

$app = new core\App();
$app->run();