<?php
$this->add('/', 'IndexController@home');

$this->add('/(?P<category>[abc]{1,1})', 'TicketsController@list');
$this->add('/(?P<category>[abc]{1,1})/(?P<ticket>[0-9]+)', 'TicketsController@ticket');
$this->add('/(?P<category>[abc]{1,1})/randt', 'TicketsController@randomTicket');
$this->add('/(?P<category>[abc]{1,1})/randq', 'TicketsController@randomQuestions');
$this->add('/(?P<category>[abc]{1,1})/check', 'TicketsController@check');