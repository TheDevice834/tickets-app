<?php
namespace app\controllers;

use core\Controller;
use app\models\Ticket;

class IndexController extends Controller {
    public function home(){
        $this->view->render('home');
    }
}