<?php
namespace app\controllers;

use core\Controller;
use app\models\Ticket;

class TicketsController extends Controller {
    public function list($category){
        $tickets = Ticket::getTickets($category);

        $this->view->render('category', [
            'title' => 'Категория '.strtoupper($category),
            'category' => $category,
            'tickets' => $tickets
        ]);
    }

    public function ticket($category, $tic){
        // Если билет решен, то...
        if($isf_result = Ticket::isFinish($tic)) return $this->view->render('ticket/resolved', [
            'title' => 'Билет решен',
            'category' => $category,
            'ticket' => $isf_result['ticket'],
            'errors' => $isf_result['errors'],
            'resolved_title' => 'Билет '.$tic
        ]);

        // Если нет номера вопроса, то перенаправляем на первый вопрос
        if(!isset($_GET['q']) || isset($_GET['q']) && $_GET['q'] == '') return redirect('?q=1');

        // Запуск билета
        $runned = Ticket::runTicket(count(Ticket::getTickets($category)), $tic);

        // Получение вопросов и проверка на существование вопроса
        $ticket = Ticket::get($category, $runned['ticket']);
        if(!isset($ticket['questions'][$_GET['q']])) return notFound();
        $question = $ticket['questions'][$_GET['q']];

        /*
            Если прогресс не соответствует номеру вопроса,
            то перенаправляем на текущий прогресс
        */
        if($runned['progress'] != $_GET['q']) return redirect('?q='.$runned['progress']);

        return $this->view->render('ticket/questions', [
            'title' => 'Билет '.$runned['ticket'],
            'category' => $category,
            'ticket' => $runned['ticket'],
            'question_number' => $_GET['q'],
            'question' => $question,
            'errors' => $runned['errors'],
            'progress' => $runned['progress']
        ]);
    }

    public function randomTicket($category){
        $ticket = rand(1, count(Ticket::getTickets($category)));
        return redirect('/'.$category.'/'.$ticket);
    }

    public function randomQuestions($category){
        // Если билет решен, то...
        if($isf_result = Ticket::isFinish(0)) return $this->view->render('ticket/resolved', [
            'title' => 'Билет решен',
            'category' => $category,
            'ticket' => $isf_result['ticket'],
            'errors' => $isf_result['errors'],
            'resolved_title' => 'Случайные вопросы'
        ]);

        // Если нет номера вопроса, то перенаправляем на первый вопрос
        if(!isset($_GET['q']) || isset($_GET['q']) && $_GET['q'] == '') return redirect('?q=1');

        // Запуск билета
        $runned = Ticket::runTicket(count(Ticket::getTickets($category)), 0);

        // Получение номера билета и номера вопроса
        $random = explode('-', $runned['rand']['questions'][$runned['progress']]);

        // Получение вопросов и проверка на существование вопроса
        $ticket = Ticket::get($category, $random[0]);
        if(!isset($ticket['questions'][$random[1]])) return notFound();
        $question = $ticket['questions'][$random[1]];

        /*
            Если прогресс не соответствует номеру вопроса,
            то перенаправляем на текущий прогресс
        */
        if($_SESSION['progress'] != $_GET['q']) return redirect('?q='.$_SESSION['progress']);

        return $this->view->render('ticket/questions', [
            'title' => 'Случайные вопросы',
            'hideticket' => true,
            'category' => $category,
            'ticket' => $random[0],
            'question_number' => $random[1],
            'question' => $question,
            'errors' => $runned['errors'],
            'progress' => $runned['progress']
        ]);
    }

    public function check($category){
        if(!isset($_GET['tic']) || isset($_GET['tic']) && $_GET['tic'] == '') return notFound();
        if(!isset($_GET['ans']) || isset($_GET['ans']) && $_GET['ans'] == '') return notFound();
        if(!isset($_GET['q']) || isset($_GET['q']) && $_GET['q'] == '') return notFound();
        if(!isset($_GET['p']) || isset($_GET['p']) && $_GET['p'] == '') return notFound();
        
        $tic = intval($_GET['tic']);
        $quest = intval($_GET['q']);
        $progress = intval($_GET['p']);
        $ans = intval($_GET['ans']);

        if($progress != $_SESSION['progress']){
            echo json_encode([
                'status' => 'resolved',
                'ticket' => $tic,
                'progress' => $_SESSION['progress']
            ]);
            return true;
        }

        $ticket = Ticket::get($category, $tic);
        $right = $ticket['right'][$quest];

        $_SESSION['progress']++;

        if($ans == $right){
            echo json_encode([
                'status' => 'success',
                'ticket' => $tic,
                'question' => $quest,
                'right' => $right
            ]);
        } else {
            $_SESSION['errors']++;
            echo json_encode([
                'status' => 'wrong',
                'ticket' => $tic,
                'question' => $quest,
                'right' => $right,
                'selected' => $ans
            ]);
        }
    }
}