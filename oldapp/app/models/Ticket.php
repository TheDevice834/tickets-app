<?php
namespace app\models;

use core\Model;

class Ticket extends Model {
    public function getTickets($category){
        $tickets = scandir(ROOT.'/tickets/'.$category);

        foreach($tickets as $key => $ticket){
            if($ticket == '..' || $ticket == '.'){
                unset($tickets[$key]);
                continue;
            }
            $tickets[$key] = explode('.', $ticket)[0];
        }

        sort($tickets, SORT_NUMERIC);

        return $tickets;
    }
    public function get($category, $ticket){
        return include(ROOT.'/tickets/'.$category.'/'.$ticket.'.php');
    }

    public function runTicket($maxtickets, $ticket){
        if(!exists_sess('ticket')
        || !exists_sess('errors')
        || !exists_sess('progress')
        || !exists_sess('rand')
        || exists_sess('ticket')
        && sess('ticket') != $ticket){
            delsess('ticket');
            delsess('errors');
            delsess('progress');
            delsess('rand');

            set_sess('ticket', $ticket);
            set_sess('errors', 0);
            set_sess('progress', 1);

            if($ticket == 0){
                set_sess('rand', [
                    'questions' => self::getRandomQuestions($maxtickets)
                ]);
            } else set_sess('rand', false);
        }

        return [
            'ticket' => sess('ticket'),
            'errors' => sess('errors'),
            'progress' => sess('progress'),
            'rand' => sess('rand')
        ];
    }

    public static function isFinish($tic){
        if($_GET['q'] == 11
        && exists_sess('progress')
        && exists_sess('ticket')
        && sess('progress') == 11
        && sess('ticket') == $tic){
            $ticket = $tic;
            $errors = sess('errors');

            delsess('ticket');
            delsess('errors');
            delsess('progress');
            delsess('rand');

            return [
                'ticket' => $ticket,
                'errors' => $errors
            ];
        } else return false;
    }

    public static function getRandomQuestions($maxtickets){
        $questions = [];
        $all = false;
        $number = 1;

        while($all == false){
            $ticket = rand(1, $maxtickets);
            $question = rand(1, 10);
            $sum = $ticket.'-'.$question;
            if(array_search($sum, $questions)) continue;
            else {
                $questions[$number] = $sum;
                $number++;
            }
            if($number > 10) $all = true;
        }
        return $questions;
    }
}