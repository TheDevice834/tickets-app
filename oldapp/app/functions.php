<?php
function notFound(){
    http_response_code(404);
    echo '404';
    die();
}

function redirect($url){
    header('Location: '.$url);
    die();
}

function sess($session){
	if(isset($_SESSION[$session])) return $_SESSION[$session];
	else return false;
}

function set_sess($session, $value){
	$_SESSION[$session] = $value;
	return true;
}

function exists_sess($session){
	if(isset($_SESSION[$session])) return true;
	else return false;
}

function delsess($session){
	unset($_SESSION[$session]);
	return true;
}