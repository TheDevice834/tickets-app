<?php
session_start();
define('APP', 'app');

if(isset($_GET['logout'])){
    if(isset($_SESSION['logged_in'])) unset($_SESSION['logged_in']);
    die(header('Location: /'));
}

if(isset($_SESSION['logged_in'])) die(header('Location: /'));
$redirect_uri = 'http://127.0.0.1:8000/login.php';

function cGet($url)
{
    $curl = curl_init();
    curl_setopt($curl,CURLOPT_URL, $url);
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($curl,CURLOPT_CONNECTTIMEOUT, 5);
    $file = curl_exec($curl);
    curl_close($curl);

    return $file;
}

if(isset($_GET['error']) && isset($_GET['error_description'])) die('Внутренняя ошибка сервера.');
else if(!isset($_GET['code'])) die(header('Location: https://oauth.vk.com/authorize?client_id=6736861&display=page&redirect_uri='.$redirect_uri.'&scope=&response_type=code&v=5.65'));

$file = cGet('https://oauth.vk.com/access_token?client_id=6736861&client_secret=Hbp76j8Fnkgivvj0SmTG&redirect_uri='.$redirect_uri.'&code='.$_GET['code']);
$access_token = json_decode($file, true);
if(isset($access_token['error']) && isset($access_token['error_description'])) die('Внутренняя ошибка сервера.');
if(!isset($access_token['access_token']) || !isset($access_token['user_id']) || !isset($access_token['expires_in'])) die('Внутренняя ошибка сервера.');

require('api/db.php');

$user = R::findOne( 'users', ' vk_id = ? ', [$access_token['user_id']]);
if(!$user){
    $file = cGet('https://api.vk.com/method/users.get?user_ids='.$access_token['user_id'].'&fields=photo_200&v=5.65&access_token='.$access_token['access_token']);
    $user_info = json_decode($file, true);

    $user = R::dispense('users');
    $user->vk_id = $access_token['user_id'];
    $user->reg_date = time();
    $user->decided = 0;
    $user->delivered = 0;
    $user->first_name = $user_info['response'][0]['first_name'];
    $user->last_name = $user_info['response'][0]['last_name'];
    $user->avatar = $user_info['response'][0]['photo_200'];
    $user_id = R::store($user);

    $user = R::findOne( 'users', ' id = ? ', [$user_id]);
}

$_SESSION['logged_in'] = $user->id;
die(header('Location: /'));