'use strict';

VK.init({
    apiId: 6079766
});

/* Controllers */
var ticketsApp = angular.module('ticketsApp', ['ngRoute', 'ngSanitize']);

ticketsApp.config(['$routeProvider', '$locationProvider', function($routeProvide, $locationProvider){
    $locationProvider.hashPrefix('');

    $routeProvide
        .when('/',{
            templateUrl: '/template/tickets-list.html',
            controller: 'TicketsListCtrl',
        })
        .when('/download',{
            templateUrl: '/template/download.html',
            controller: 'DownloadCtrl',
        })
        .when('/about',{
            templateUrl: '/template/about.html',
            controller: 'AboutCtrl',
        })
        .when('/ticket/:ticketId',{
            templateUrl: '/template/ticket.html',
            controller: 'TicketCtrl',
        })
        .when('/user-:userId',{
            templateUrl: '/template/user.html',
            controller: 'UserCtrl',
        })
        .when('/rating',{
            templateUrl: '/template/rating.html',
            controller: 'RatingCtrl',
        })
        .when('/news',{
            templateUrl: '/template/news.html',
            controller: 'NewsCtrl',
        })
        .otherwise({
            redirectTo: '/'
        });

}]);

ticketsApp.controller('TicketsListCtrl', ['$scope', '$http', '$location', function($scope, $http, $location){
    $scope.random = Math.floor(Math.random() * (15 - 1 + 1)) + 1;

    $http({
        method: 'GET',
        url: '/resource/tickets.php'
    }).then(function successCallback(response){
        $scope.tickets = response.data;
    });
}]);

ticketsApp.controller('DownloadCtrl', ['$scope', '$http', '$location', function($scope, $http, $location){

}]);

ticketsApp.controller('AboutCtrl', ['$scope', '$http', '$location', function($scope, $http, $location){

}]);

ticketsApp.controller('NavCtrl', ['$scope', '$location', '$http', function($scope, $location, $http){
    $scope.isActive = function(viewLocation){
        return viewLocation === $location.path();
    };

    $http({
        method: 'GET',
        url: '/api/account.php?act=is-auth'
    }).then(function successCallback(response){
        if(response.data.error === undefined){
            $scope.user = response.data;
            $scope.isAuth = true;
        }
        else $scope.isAuth = false;
    });

    $scope.isAuth = true;
}]);

ticketsApp.controller('TicketCtrl', ['$scope', '$http', '$location', '$routeParams', '$sce', function($scope, $http, $location, $routeParams, $sce){
    $scope.trustedHtml = function(snippet) {
        return $sce.trustAsHtml(snippet);
    };

    $scope.randomTicket = function(){
        $scope.ticketId = Math.floor(Math.random() * (15 - 1 + 1)) + 1;
    };

    $scope.randomQuestion = function(){
        $scope.question = Math.floor(Math.random() * (10 - 1 + 1)) + 1;
    };

    if($routeParams.ticketId < 0 || $routeParams.ticketId > 15) $location.path('/');
    if(parseInt($routeParams.ticketId) === 0){
        $scope.isRandom = true;
        $scope.randomTicket();
        $scope.randomQuestion();
        $scope.caption = 'Случайные вопросы';
    }
    else
    {
        $scope.isRandom = false;
        $scope.ticketId = $routeParams.ticketId;
        $scope.question = 1;
        $scope.caption = 'Билет #'+$scope.ticketId;
    }

    $scope.progressQuestion = 1;
    $scope.errors = 0;
    $scope.answered = false;
    $scope.isContinue = false;
    $scope.isFinal = false;

    for(var i = 1; i <= 4; i++){
        angular.element(document.querySelector('#reply_'+i)).removeClass('btn-success');
        angular.element(document.querySelector('#reply_'+i)).removeClass('btn-danger');
        angular.element(document.querySelector('#reply_'+i)).addClass('btn-primary');
    }

    angular.element(document.querySelector('#final_text')).removeClass('alert-success');
    angular.element(document.querySelector('#final_text')).removeClass('alert-warning');
    angular.element(document.querySelector('#final_text')).removeClass('alert-danger');

    $scope.reply = function(answer){
        if($scope.answered) return $scope.continue();

        if(parseInt(answer) === $scope.tickets[$scope.ticketId][$scope.question].right){
            angular.element(document.querySelector('#reply_'+answer)).removeClass('btn-primary');
            angular.element(document.querySelector('#reply_'+answer)).addClass('btn-success');
        }
        else
        {
            angular.element(document.querySelector('#reply_'+answer)).removeClass('btn-primary');
            angular.element(document.querySelector('#reply_'+answer)).addClass('btn-danger');

            angular.element(document.querySelector('#reply_'+$scope.tickets[$scope.ticketId][$scope.question].right)).removeClass('btn-primary');
            angular.element(document.querySelector('#reply_'+$scope.tickets[$scope.ticketId][$scope.question].right)).addClass('btn-success');

            $scope.errors++;
        }

        $scope.answered = true;
        $scope.isContinue = true;

    };

    $scope.continue = function(){
        if($scope.answered){
            if($scope.progressQuestion < 10){
                $scope.progressQuestion++;
                if(!$scope.isRandom) $scope.question++;
                else
                {
                    $scope.randomTicket();
                    $scope.randomQuestion();
                }
            }
            else return $scope.final();

            for(var i = 1; i <= 4; i++){
                angular.element(document.querySelector('#reply_'+i)).removeClass('btn-success');
                angular.element(document.querySelector('#reply_'+i)).removeClass('btn-danger');
                angular.element(document.querySelector('#reply_'+i)).addClass('btn-primary');
            }

            $scope.answered = false;
            $scope.isContinue = false;
        }
    };

    $scope.final = function(){
        $scope.isFinal = true;
        $scope.answered = false;
        $scope.isContinue = false;

        $http({
            method: 'GET',
            url: '/api/account.php?act=decided'
        });

        if($scope.errors < 1){
            angular.element(document.querySelector('#final_text')).addClass('alert-success');
            $scope.finalText = 'Билет сдан.';

            $http({
                method: 'GET',
                url: '/api/account.php?act=delivered'
            });
        }
        if($scope.errors === 1){
            angular.element(document.querySelector('#final_text')).addClass('alert-warning');
            $scope.finalText = 'Билет сдан.';

            $http({
                method: 'GET',
                url: '/api/account.php?act=delivered'
            });
        }
        if($scope.errors > 1){
            angular.element(document.querySelector('#final_text')).addClass('alert-danger');
            $scope.finalText = 'Билет не сдан.';
        }
    };

    $http({
        method: 'GET',
        url: '/resource/tickets.php'
    }).then(function successCallback(response){
        $scope.tickets = response.data;
    });
}]);

ticketsApp.controller('UserCtrl', ['$scope', '$location', '$routeParams', '$http', function($scope, $location, $routeParams, $http){
    $scope.user_id = $routeParams.userId;

    $http({
        method: 'GET',
        url: '/api/account.php?act=get&user_id='+$scope.user_id
    }).then(function successCallback(response){
        $scope.user = response.data;

        VK.Api.call('users.get', {user_ids: $scope.user.vk_id, fields: 'photo_max'}, function(r) {
            if(r.response) {
                $scope.user.vk = r.response[0];
            }
        });

        if($scope.user.id === undefined) $location.path('/');
    });
}]);

ticketsApp.controller('RatingCtrl', ['$scope', '$location', '$routeParams', '$http', function($scope, $location, $routeParams, $http){
    $scope.usersFilter = 'decided';

    $scope.updateList = function(filter){
        $scope.usersFilter = filter;

        $http({
            method: 'GET',
            url: '/api/account.php?act=users&sort='+filter
        }).then(function successCallback(response){
            $scope.users = response.data;
        });
    };

    $scope.updateList($scope.usersFilter);
}]);

ticketsApp.controller('NewsCtrl', ['$scope', '$location', '$routeParams', '$http', function($scope, $location, $routeParams, $http){

}]);
