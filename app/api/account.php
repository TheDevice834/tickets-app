<?php
session_start();
define('APP', 'app');

require('db.php');

if(isset($_GET['act']) && $_GET['act'] == "is-auth"){
    if(isset($_SESSION['logged_in'])){
        $user = R::getRow( 'select * from users where id = :id', [':id' => $_SESSION['logged_in']]);
        echo json_encode($user);
    }
    else echo json_encode(['error' => 'not auth']);
}
if(isset($_GET['act']) && $_GET['act'] == "get"){
    if(isset($_GET['user_id'])){
        $user = R::getRow( 'select * from users where id = :id', [':id' => $_GET['user_id']]);
        $user['reg_date'] = date('d.m.Y в H:i:s', $user['reg_date']);
        echo json_encode($user);
    }
}
if(isset($_GET['act']) && $_GET['act'] == "decided"){
    if(isset($_SESSION['logged_in'])){
        $user = R::findOne( 'users', ' id = ? ', [$_SESSION['logged_in']]);
        $user->decided++;
        R::store($user);
    }
}
if(isset($_GET['act']) && $_GET['act'] == "delivered"){
    if(isset($_SESSION['logged_in'])){
        $user = R::findOne( 'users', ' id = ? ', [$_SESSION['logged_in']]);
        $user->delivered++;
        R::store($user);
    }
}

if(isset($_GET['act']) && $_GET['act'] == "users"){
    if(isset($_GET['sort'])){
        if(strval($_GET['sort']) == 'decided') $users = R::getAll('select * from users order by decided desc');
        else if(strval($_GET['sort']) == 'decided') $users = R::getAll('select * from users order by delivered desc');
        else die('error');

        echo json_encode($users);
    }
}