import Vue from 'vue'
import VueRouter from 'vue-router'
import NProgress from 'nprogress'

import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Category from '../views/Category.vue'
import Ticket from '../views/Ticket.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', name: 'home', component: Home },
  { path: '/about', name: 'about', component: About },
  { path: '/:category', name: 'category', component: Category },
  { path: '/:category/ticket-:ticket', name: 'ticket', component: Ticket },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeResolve((to, from, next) => {
  if(to.name){
    NProgress.start()
  }
  next()
})

router.afterEach((to, from) => {
  NProgress.done()
})

export default router
