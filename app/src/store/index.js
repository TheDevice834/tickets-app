import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import Tickets from '../assets/tickets.json'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

export default new Vuex.Store({
  state: {
    tickets: Tickets,
    ticketInfo: { category: null, ticket: null, progress: null, errors: null }
  },
  mutations: {
    startNewTicket(state, ticketInfo){
      state.ticketInfo = { category: ticketInfo.category, ticket: ticketInfo.ticket, progress: 1, errors: 0 }
    },
    addProgress(state, error = false){
      if(error) state.ticketInfo.errors++;
      state.ticketInfo.progress++;
    },
    finishTicket(state){
      state.ticketInfo = { category: null, ticket: null, progress: null, errors: null }
    }
  },
  actions: {
  },
  modules: {},
  plugins: [vuexLocal.plugin]
})
